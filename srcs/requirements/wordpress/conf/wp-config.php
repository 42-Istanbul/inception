<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv('DB_NAME') );

/** Database username */
define( 'DB_USER', getenv('DB_USER') );

/** Database password */
define( 'DB_PASSWORD', getenv('DB_PASSWORD') );

/** Database hostname */
define( 'DB_HOST', getenv('DB_HOST') );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D-KiX%B4-9>Df4F*II?F/oMoy$X%jE<ptk3C*dI+zxujWt*/bmUx%j=+Y*hJxH:_');
define('SECURE_AUTH_KEY',  '.E;lT-jlVTP5yqeHI5#2[4!.};&TN5^WZ`Dq^W[kGM@MXDVNJx[2KERSli}nY-_z');
define('LOGGED_IN_KEY',    'q-Ut}Hr8us*-VP20Duyuoz&T]-#UI*l!`PXyD&lLg92V],|=Yj]B+GjF1yW3;}W_');
define('NONCE_KEY',        'F%YU5-xfT6<8^ %PWrz??-N%9wGh%Qm!A&BdP_D@D!GJ5;U|f.OGs1#DX*AyUFW7');
define('AUTH_SALT',        '|1.gMat&@Q=]nO6+9D/;(b-pU:_6(;^^wK*@jBDp36N2!4Eu+^?H68I6cu/!hjp?');
define('SECURE_AUTH_SALT', 'pUvoI]+i/@%-fpmH_rxfnx3--iT[ xT*O;}107!y1YO&)4W)BM[@cmSe3`62}<|v');
define('LOGGED_IN_SALT',   '_3b -qagKFd>LsmfwEi@;NplE^kn( S2|}nC0]+TJQoyJO1f:p_xK(2$snFm(S_J');
define('NONCE_SALT',       'G*]+tsJN(Y>|EYA08M+.f+uck=Ux.tK6W==!h:KzxU-&sf-sw1o|6/.kbx*St22u');

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';